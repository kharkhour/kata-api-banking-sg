package application.banking.kata.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import application.banking.kata.business.BankAccountBusiness;
import application.banking.kata.dto.BankAccountDto;
import application.banking.kata.dto.TransactionDto;
import application.banking.kata.exception.controllers.AccountNotFoundException;
import application.banking.kata.exception.controllers.AccountNumberFormatException;
import application.banking.kata.exception.controllers.AccountSentIsNullException;
import application.banking.kata.exception.controllers.AccountSentNotASavingAccount;
import application.banking.kata.exception.controllers.AmountDepositNotStriclyPositiveException;
import application.banking.kata.exception.controllers.InvalidDepositValuesException;
import application.banking.kata.exception.controllers.TransactionIsNullException;

@RestController
public class BankAccountController {

	@Autowired
	private BankAccountBusiness bankAccountBusiness;

	@PostMapping("/accounts/{idAccount}")
	public BankAccountDto depositAmount(@RequestBody BankAccountDto account)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException {
		return bankAccountBusiness.deposit(account);
	}

	@PutMapping("/accounts/{idAccount}")
	public BankAccountDto withDrawAmount(@RequestBody BankAccountDto account) throws AccountSentIsNullException,
			AccountSentNotASavingAccount, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException {
		return bankAccountBusiness.withdraw(account);
	}

	@GetMapping("/accounts/{idAccount}/transactions")
	public List<TransactionDto> getTransactionsOfSpecificAccount(@RequestBody BankAccountDto account)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException {
		return bankAccountBusiness.getTransactionsOfSpecificAccount(account);
	}
	
}
