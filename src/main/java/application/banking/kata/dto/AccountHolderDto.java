package application.banking.kata.dto;

import java.util.List;

public class AccountHolderDto {

	private Long idAccountHolder;

	private String accountHolderName;

	private String accountHolderSurname;

	private List<TransactionDto> transactions;

	private List<BankAccountDto> bankAccounts;

	public Long getIdAccountHolder() {
		return idAccountHolder;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderSurname() {
		return accountHolderSurname;
	}

	public void setAccountHolderSurname(String accountHolderSurname) {
		this.accountHolderSurname = accountHolderSurname;
	}

	public List<TransactionDto> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionDto> transactions) {
		this.transactions = transactions;
	}

	public List<BankAccountDto> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(List<BankAccountDto> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

}
