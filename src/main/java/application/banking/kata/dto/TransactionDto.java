package application.banking.kata.dto;

import java.time.LocalDateTime;

import application.banking.kata.dao.transactions.PermittedTransactions;

public class TransactionDto {

	private Long idTransaction;

	private PermittedTransactions transactionLibeller;

	private float amount;

	private LocalDateTime transactionDate;

	private float balance;

	private AccountHolderDto accountHolder;

	private BankAccountDto account;

	public Long getIdTransaction() {
		return idTransaction;
	}

	public PermittedTransactions getTransactionLibeller() {
		return transactionLibeller;
	}

	public void setTransactionLibeller(PermittedTransactions transactionLibeller) {
		this.transactionLibeller = transactionLibeller;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public AccountHolderDto getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(AccountHolderDto accountHolder) {
		this.accountHolder = accountHolder;
	}

	public BankAccountDto getAccount() {
		return account;
	}

	public void setAccount(BankAccountDto account) {
		this.account = account;
	}

}
