package application.banking.kata.dto;

import java.util.Date;
import java.util.List;

public class BankAccountDto {

	private Long idAccount;

	private String accountNumber;

	private Date date;

	private float balance;

	protected static double interestRate = 0;

	private AccountHolderDto accountHolder;

	private List<TransactionDto> transactions;

	public Long getIdAccount() {
		return idAccount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public static double getInterestRate() {
		return interestRate;
	}

	public AccountHolderDto getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(AccountHolderDto accountHolder) {
		this.accountHolder = accountHolder;
	}

	public List<TransactionDto> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionDto> transactions) {
		this.transactions = transactions;
	}

	public BankAccountDto(String accountNumber, Date date, float balance, AccountHolderDto accountHolder) {
		super();
		this.accountNumber = accountNumber;
		this.date = date;
		this.balance = balance;
		this.accountHolder = accountHolder;
	}

	public BankAccountDto() {

	}

}
