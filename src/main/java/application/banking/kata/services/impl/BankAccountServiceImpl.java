package application.banking.kata.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import application.banking.kata.dao.accounts.BankAccount;
import application.banking.kata.dao.transactions.PermittedTransactions;
import application.banking.kata.dao.transactions.Transaction;
import application.banking.kata.services.BankAccountService;

@Service
public class BankAccountServiceImpl implements BankAccountService {

	private BankAccount deposit(BankAccount account) {
		// repository not immlemented yet
		return null;
	}

	private BankAccount withdraw(BankAccount account) {
		// repository not immlemented yet
		return null;
	}

	@Override
	public BankAccount launchOperation(BankAccount account, String transactionType) {

		if (transactionType.equals(PermittedTransactions.DEPOSIT.name()))
			deposit(account);

		withdraw(account);

		// repository not immlemented yet
		return null;

	}

	@Override
	public List<Transaction> getTransactionsOfSpecificAccount(BankAccount account) {
		// repository not immlemented yet
		return null;

	}

	@Override
	public BankAccount isMyAccount(Long idAccount, Long idAccountHolder) {
		// TODO Auto-generated method stub
		return null;
	}

}
