package application.banking.kata.services;

import java.util.List;

import application.banking.kata.dao.accounts.BankAccount;
import application.banking.kata.dao.transactions.Transaction;

public interface BankAccountService {
	BankAccount launchOperation(BankAccount account, String transactionType);

	BankAccount isMyAccount(Long idAccount, Long idAccountHolder);

	List<Transaction> getTransactionsOfSpecificAccount(BankAccount account);

}
