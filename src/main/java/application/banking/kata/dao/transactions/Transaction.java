package application.banking.kata.dao.transactions;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import application.banking.kata.dao.AccountHolder;
import application.banking.kata.dao.accounts.BankAccount;

@Entity
public class Transaction implements Serializable {

	private static final long serialVersionUID = 1L;

	private @Id @GeneratedValue Long idTransaction;

	@Enumerated(EnumType.STRING)
	private PermittedTransactions transactionLibeller;

	private float amount;
	
	private float balance; //balance after each operation for history purposes , initial value should be the same as account balance

	private LocalDateTime transactionDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_holder_id")
	private AccountHolder accountHolder;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bank_account_id")
	@JsonIgnoreProperties("account")  //to prevent circular objects when retrieving list of transactions via Client REST 
	private BankAccount account;

	public Long getIdTransaction() {
		return idTransaction;
	}

	public PermittedTransactions getTransactionLibeller() {
		return transactionLibeller;
	}

	public void setTransactionLibeller(PermittedTransactions transactionLibeller) {
		this.transactionLibeller = transactionLibeller;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}
	
	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public LocalDateTime getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDateTime transactionDate) {
		this.transactionDate = transactionDate;
	}

	public AccountHolder getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(AccountHolder accountHolder) {
		this.accountHolder = accountHolder;
	}

	public BankAccount getAccount() {
		return account;
	}

	public void setAccount(BankAccount account) {
		this.account = account;
	}

}
