package application.banking.kata.dao.transactions;

public enum PermittedTransactions {

	WITHDRAW, 
	DEPOSIT
	
}
