package application.banking.kata.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import application.banking.kata.dao.accounts.BankAccount;
import application.banking.kata.dao.transactions.Transaction;

@Entity
public class AccountHolder implements Serializable {

	private static final long serialVersionUID = 1L;

	private @Id @GeneratedValue Long idAccountHolder;

	private String accountHolderName;

	private String accountHolderSurname;

	@OneToMany(mappedBy = "accountHolder", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Transaction> transactions;

	@OneToMany(mappedBy = "accountHolder", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BankAccount> bankAccounts;

	public Long getIdAccountHolder() {
		return idAccountHolder;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderSurname() {
		return accountHolderSurname;
	}

	public void setAccountHolderSurname(String accountHolderSurname) {
		this.accountHolderSurname = accountHolderSurname;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public List<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(List<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

}
