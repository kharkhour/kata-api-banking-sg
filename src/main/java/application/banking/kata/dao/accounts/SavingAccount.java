package application.banking.kata.dao.accounts;

import java.time.LocalDateTime;

import application.banking.kata.dao.AccountHolder;

public class SavingAccount extends BankAccount {

	private static final long serialVersionUID = 1L;

	static {
		interestRate = 0.025;
	}

	public SavingAccount(String accountNumber, LocalDateTime date, float balance, AccountHolder accountHolder) {
		super(accountNumber, date, balance, accountHolder);
	}

	public SavingAccount() {
		super();
	}
	
	
	

}
