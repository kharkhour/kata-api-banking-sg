package application.banking.kata.dao.accounts;

import java.time.LocalDateTime;

import application.banking.kata.dao.AccountHolder;

public class TransactionAccount extends BankAccount {

	private static final long serialVersionUID = 1L;

	public TransactionAccount(String accountNumber, LocalDateTime date, float balance, AccountHolder accountHolder) {
		super(accountNumber, date, balance, accountHolder);
	}
	
	public TransactionAccount() {
		super();
	}

}
