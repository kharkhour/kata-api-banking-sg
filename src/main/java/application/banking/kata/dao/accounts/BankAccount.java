package application.banking.kata.dao.accounts;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import application.banking.kata.dao.AccountHolder;
import application.banking.kata.dao.transactions.Transaction;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "accountNumber" }))
public class BankAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	private @Id @GeneratedValue Long idAccount;

	private String accountNumber;

	private LocalDateTime date;

	private float balance;

	protected static double interestRate = 0;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_holder_id")
	private AccountHolder accountHolder;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Transaction> transactions;

	public Long getIdAccount() {
		return idAccount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(final LocalDateTime date) {
		this.date = date;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(final float balance) {
		this.balance = balance;
	}

	public AccountHolder getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(final AccountHolder accountHolder) {
		this.accountHolder = accountHolder;
	}

	public BankAccount(final String accountNumber, final LocalDateTime date, final float balance,
			final AccountHolder accountHolder) {
		this.accountNumber = accountNumber;
		this.date = date;
		this.balance = balance;
		this.accountHolder = accountHolder;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public BankAccount() {
	}

}
