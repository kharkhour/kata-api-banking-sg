package application.banking.kata.business.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import application.banking.kata.business.BankAccountBusiness;
import application.banking.kata.dao.accounts.BankAccount;
import application.banking.kata.dao.transactions.Transaction;
import application.banking.kata.dto.BankAccountDto;
import application.banking.kata.dto.SavingAccountDto;
import application.banking.kata.dto.TransactionDto;
import application.banking.kata.exception.controllers.AccountNotFoundException;
import application.banking.kata.exception.controllers.AccountNumberFormatException;
import application.banking.kata.exception.controllers.AccountSentIsNullException;
import application.banking.kata.exception.controllers.AccountSentNotASavingAccount;
import application.banking.kata.exception.controllers.AmountDepositNotStriclyPositiveException;
import application.banking.kata.exception.controllers.InvalidDepositValuesException;
import application.banking.kata.exception.controllers.TransactionIsNullException;
import application.banking.kata.mapper.dto.dao.BankAccountMapper;
import application.banking.kata.mapper.dto.dao.TransactionMapper;
import application.banking.kata.services.BankAccountService;

@Component
public class BankAccountBusinessImpl implements BankAccountBusiness {

	@Autowired
	private BankAccountService bankAccountService;

	@Override
	public BankAccountDto withdraw(BankAccountDto bankAccountDto) throws AccountSentIsNullException,
			AccountSentNotASavingAccount, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException {

		Optional<BankAccountDto> bankAccountOptional = Optional.ofNullable(bankAccountDto);
		bankAccountOptional.orElseThrow(AccountSentIsNullException::new);

		bankAccountOptional.filter(bankAccount -> bankAccount instanceof SavingAccountDto)
				.orElseThrow(AccountSentNotASavingAccount::new);

		BankAccount account = commonValidationSteps(bankAccountDto);

		account = bankAccountService.launchOperation(account,
				bankAccountDto.getTransactions().get(0).getTransactionLibeller().toString());

		BankAccountDto NewAccountToReturn = BankAccountMapper.INSTANCE.BankAccountToBankAccountDto(account);

		return NewAccountToReturn;

	}

	@Override
	public BankAccountDto  deposit(BankAccountDto bankAccountDto)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException {

		BankAccount account = commonValidationSteps(bankAccountDto);
		account = bankAccountService.launchOperation(account,
				bankAccountDto.getTransactions().get(0).getTransactionLibeller().toString());

		BankAccountDto NewAccountToReturn = BankAccountMapper.INSTANCE.BankAccountToBankAccountDto(account);

		return NewAccountToReturn;
	}

	private void isMyAccount(BankAccountDto bankAccount) throws AccountSentIsNullException, AccountNotFoundException {

		Optional<BankAccountDto> OptionalbankAccount = Optional.ofNullable(bankAccount);

		OptionalbankAccount.orElseThrow(AccountSentIsNullException::new);

		BankAccount accountToFind = BankAccountMapper.INSTANCE.BankAccountDtoToBankAccount(bankAccount);

		Optional.ofNullable(bankAccountService.isMyAccount(accountToFind.getIdAccount(),
				accountToFind.getAccountHolder().getIdAccountHolder())).orElseThrow(AccountNotFoundException::new);

	}

	private void isTransactionListNotNull(BankAccountDto bankAccountDto) throws TransactionIsNullException {
		Optional.ofNullable(bankAccountDto.getTransactions()).orElseThrow(TransactionIsNullException::new);
	}

	private void AreAmountAndAccountNumberValids(String accountNumber, float amount)
			throws InvalidDepositValuesException, AccountNumberFormatException,
			AmountDepositNotStriclyPositiveException {

		Optional<String> optionalAccountNumber = Optional.ofNullable(accountNumber);

		optionalAccountNumber.filter(acc -> StringUtils.isNotEmpty(acc) && StringUtils.isAlphanumeric(acc))
				.orElseThrow(InvalidDepositValuesException::new);

		optionalAccountNumber.filter(acc -> StringUtils.isNotEmpty(acc)).orElseThrow(AccountNumberFormatException::new);

		Optional<Float> amountOptional = Optional.of(amount);

		amountOptional.filter(a -> a > 0).orElseThrow(AmountDepositNotStriclyPositiveException::new);

	}

	private void setNewBalance(BankAccountDto bankAccountDto, TransactionDto transactionDto) {
		float newBalance = bankAccountDto.getBalance() + transactionDto.getAmount();
		bankAccountDto.getTransactions().get(0).setBalance(newBalance);
		bankAccountDto.setBalance(newBalance);
	}

	private BankAccount commonValidationSteps(BankAccountDto bankAccountDto)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException {

		isMyAccount(bankAccountDto);

		isTransactionListNotNull(bankAccountDto);

		TransactionDto transactionDto = bankAccountDto.getTransactions().get(0);

		AreAmountAndAccountNumberValids(bankAccountDto.getAccountNumber(),
				Optional.of(transactionDto).get().getAmount());

		setNewBalance(bankAccountDto, transactionDto);

		BankAccount account = BankAccountMapper.INSTANCE.BankAccountDtoToBankAccount(bankAccountDto);
		return account;
	}

	@Override
	public List<TransactionDto> getTransactionsOfSpecificAccount(BankAccountDto bankAccount)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException {

		isMyAccount(bankAccount);

		isTransactionListNotNull(bankAccount);

		BankAccount account = BankAccountMapper.INSTANCE.BankAccountDtoToBankAccount(bankAccount);

		List<Transaction> transactions = bankAccountService.getTransactionsOfSpecificAccount(account);
		List<TransactionDto> transactionsToReturn = TransactionMapper.INSTANCE
				.TransactionListsToTransactionDtoList(transactions);

		return transactionsToReturn;
	}

}
