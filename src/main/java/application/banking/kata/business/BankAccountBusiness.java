package application.banking.kata.business;

import java.util.List;

import application.banking.kata.dto.BankAccountDto;
import application.banking.kata.dto.TransactionDto;
import application.banking.kata.exception.controllers.AccountNotFoundException;
import application.banking.kata.exception.controllers.AccountNumberFormatException;
import application.banking.kata.exception.controllers.AccountSentIsNullException;
import application.banking.kata.exception.controllers.AccountSentNotASavingAccount;
import application.banking.kata.exception.controllers.AmountDepositNotStriclyPositiveException;
import application.banking.kata.exception.controllers.InvalidDepositValuesException;
import application.banking.kata.exception.controllers.TransactionIsNullException;

public interface BankAccountBusiness {

	BankAccountDto deposit(BankAccountDto account)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException;

	BankAccountDto withdraw(BankAccountDto bankAccountDto) throws AccountSentIsNullException,
			AccountSentNotASavingAccount, AccountNotFoundException, TransactionIsNullException,
			InvalidDepositValuesException, AccountNumberFormatException, AmountDepositNotStriclyPositiveException;

	List<TransactionDto> getTransactionsOfSpecificAccount(BankAccountDto bankAccount)
			throws AccountSentIsNullException, AccountNotFoundException, TransactionIsNullException;

}
