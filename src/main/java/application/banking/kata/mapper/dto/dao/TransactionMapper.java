package application.banking.kata.mapper.dto.dao;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import application.banking.kata.dao.transactions.Transaction;
import application.banking.kata.dto.TransactionDto;

@Mapper(componentModel="spring")
public interface TransactionMapper {

	TransactionMapper INSTANCE = Mappers.getMapper( TransactionMapper.class ); 
	 
    TransactionDto TransactionToTransactionDto(Transaction transaction);

    Transaction TransactionDtoToTransaction(TransactionDto transaction);

	List<TransactionDto> TransactionListsToTransactionDtoList(List<Transaction> transactions); 
}
