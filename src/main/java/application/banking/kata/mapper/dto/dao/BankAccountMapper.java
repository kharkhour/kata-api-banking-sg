package application.banking.kata.mapper.dto.dao;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import application.banking.kata.dao.accounts.BankAccount;
import application.banking.kata.dto.BankAccountDto;

@Mapper(componentModel="spring")
public interface BankAccountMapper {

	BankAccountMapper INSTANCE = Mappers.getMapper(BankAccountMapper.class);

	BankAccountDto BankAccountToBankAccountDto(BankAccount account);

	BankAccount BankAccountDtoToBankAccount(BankAccountDto account);

	List<BankAccount> ListOfBankAccountDtosToListOfBankAccount(List<BankAccountDto> account);

	List<BankAccountDto> ListOfBankAccountToListOfBankAccountDtos(List<BankAccount> account);

}
