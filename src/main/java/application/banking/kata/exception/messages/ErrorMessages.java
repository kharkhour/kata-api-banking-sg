package application.banking.kata.exception.messages;

public interface ErrorMessages {

	String ACCOUNT_NOT_FOUND_EXCEPTION_MESSAGE = "The Account you are looking for is not available";
	String ACCOUNT_SEND_NOT_A_SAVING_EXCEPTION_MESSAGE = "The account you are trying to update is not a saving account";
	String ACCOUNT_SEND_IS_NULL_EXCEPTION_MESSAGE = "The account sent is null, please send a filled one";
	String INVALID_DEPOSIT_VALUES_EXCEPTION_MESSAGE = "Either the amount and/or the accountNumber provided do not respect the pattern rules ";
	String TRANSACTION_IS_NULL_EXCEPTION_MESSAGE = "the transaction sent is null , please send a filled one";

}
