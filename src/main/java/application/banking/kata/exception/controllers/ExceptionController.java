package application.banking.kata.exception.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import application.banking.kata.exception.messages.ErrorMessages;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AccountSentIsNullException.class)
	public void handleAccountSentIsNullException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_ACCEPTABLE.value(), ErrorMessages.ACCOUNT_SEND_IS_NULL_EXCEPTION_MESSAGE);
	}

	@ExceptionHandler(AccountNotFoundException.class)
	public void handleAccountNotFoundException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_FOUND.value(), ErrorMessages.ACCOUNT_NOT_FOUND_EXCEPTION_MESSAGE);
	}

	@ExceptionHandler(AccountSentNotASavingAccount.class)
	public void handleAccountSentNotASavingAccountException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_ACCEPTABLE.value(),
				ErrorMessages.ACCOUNT_SEND_NOT_A_SAVING_EXCEPTION_MESSAGE);
	}

	@ExceptionHandler(InvalidDepositValuesException.class)
	public void handleInvalidDepositValuesException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_ACCEPTABLE.value(), ErrorMessages.INVALID_DEPOSIT_VALUES_EXCEPTION_MESSAGE);
	}

	@ExceptionHandler(TransactionIsNullException.class)
	public void handleTransactionIsNullException(HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.NOT_ACCEPTABLE.value(), ErrorMessages.TRANSACTION_IS_NULL_EXCEPTION_MESSAGE);
	}

}