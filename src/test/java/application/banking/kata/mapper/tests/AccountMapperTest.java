package application.banking.kata.mapper.tests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import application.banking.kata.dao.AccountHolder;
import application.banking.kata.dao.accounts.BankAccount;
import application.banking.kata.dto.AccountHolderDto;
import application.banking.kata.dto.BankAccountDto;
import application.banking.kata.mapper.dto.dao.BankAccountMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountMapperTest {

	@Test
	public void shouldMapAccountToDto() {

		AccountHolder accountHolder = new AccountHolder();
		accountHolder.setAccountHolderName("zaka");

		BankAccount account = new BankAccount("AA11BB", null, 5, null);

		BankAccountDto accountDto = BankAccountMapper.INSTANCE.BankAccountToBankAccountDto(account);

		Assert.assertNotNull(accountDto);
		Assert.assertEquals(accountDto.getAccountNumber(), "AA11BB");
	}

	@Test
	public void shouldMapDtoToAccount() {

		AccountHolderDto accountHolderDto = new AccountHolderDto();
		accountHolderDto.setAccountHolderName("zaka");

		BankAccountDto accountDto = new BankAccountDto("AA11BB", null, 22, accountHolderDto);

		BankAccount account = BankAccountMapper.INSTANCE.BankAccountDtoToBankAccount(accountDto);

		Assert.assertNotNull(account);
		Assert.assertEquals(account.getAccountNumber(), "AA11BB");
		Assert.assertEquals(account.getAccountHolder().getAccountHolderName(), "zaka");
	}

}
